
| event name | attendee total | host/leader | teacher/speaker | co-teacher / co-speaker / co-host | date | location | address | url |
| ------------- |:-------------:|:----------:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Learn Linux! | 21 | Anthony Sayre | David Willson | Anthony Sayre | 1/28/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/236674759/ |
| Learn Linux - Samba file-server, Apache web-server, DigitalOcean Cloud | 21 | Anthony Sayre | David Willson | Anthony Sayre | 2/16/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/237409515/ |
| Learn Linux – Catch-Up Day | 13 | Anthony Sayre | Erin Huffaker| Anthony Sayre | 3/5/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/237913855/ |
| Learn Linux – Digital Ocean (Cloud), and DNS | 13 | Anthony Sayre | David Willson| Anthony Sayre | 3/11/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/237967510/ |
| Learn Docker! | 18 | Anthony Sayre | Aaron Brown | NA | 4/8/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/238041670/ |
| Learn Ansible! | 16 | Anthony Sayre | Anthony Sayre | Zach Mance | 5/13/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/239187416/ |
| Learn Git And Markdown! | 8 | Anthony Sayre | David Willson | NA | 6/10/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/239305225/ |
| Learn Kubernetes! | 17 | Dusty Smith | Marcial White | NA | 7/8/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/240730753/ |
| Learn SQL | 11 | Anthony Sayre | Chris Fedde | NA | 9/9/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/242566199/ |
| Learn Openstack | 12 | Anthony Sayre | David Willson | NA | 10/14/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/243488239/ |
| Ansible/Linux Basics Review | 8 | Anthony Sayre | Anthony Sayre | NA | 11/11/17 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/244662128/ |
| Ansible/Linux Basics Review – WORKSHOP | 16 | Anthony Sayre | Anthony Sayre | NA | 1/13/18 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/246529655/ |
| Red Hat Satellite 6 – Introduction and Feature Roadmap | 16 | Chris Fedde | Rich Jerrido | Anthony Sayre | 1/23/18 | ASG | 5800 South Quebec Street, Greenwood Village CO, 80111 | https://www.meetup.com/it-ntl/events/246235265/ |
| Ansible Docker Challenge - WORKSHOP | 11 | Anthony Sayre | Anthony Sayre | NA | 2/24/18 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/247750297/ |
| Ansible Tower - auto-configure and integrate legacy and cloud resources | 12 | Anthony Sayre | Chad Rodgers | NA | 3/13/18 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/247874131/ |
| What Is DevOps and Why Does It Matter? | 14 | Women Who Code / Nowai Matthews | Silvia Briscoe | Anthony Sayre | 4/4/18 | SendGrid | 1801 California St #500, Denver CO, 80202 | https://www.meetup.com/Women-Who-Code-Boulder-Denver/events/hzbfkpyxgbgb/ |
| Microservices And Container Management Front And Back | 16 | Anthony Sayre | Jim Garrett | Travis Nelson | 4/19/18 | Alliance Center | 1536 Wynkoop Street, Denver CO, 80202 | https://www.meetup.com/it-ntl/events/249136724/ |
| Ansible Workshop | 12 | Women Who Code / Nowai Matthews | Silvia Briscoe | Anthony Sayre | 5/2/18 | SendGrid | 1801 California St #500, Denver CO, 80202 | https://www.meetup.com/Women-Who-Code-Boulder-Denver/events/qxbfkpyxhbdb/ |
| CUSTOMER AND PARTNER happy hour / Spatial Corp(Dessault) | 15 | Anthony Sayre | Anthony Sayre | Dusty Smith | 5/4/18 | Wonderland Brewery | 5913, 5450 W 120th Ave, Broomfield, CO 80020 | NA |
| CLIENT ON-SITE – Red Hat – Openshift / Micro Services – on-site talk/demo at Epsilon | 11 | Anthony Sayre | Red Hat | Dave Bratton | 6/5/18 | Epsilon | 11030 Circle Point Rd Suite 110, Westminster CO, 80020 | NA |
| Kubernetes on Cloud w/ Kops | 14 | Anthony Sayre | Marcial White | NA | 6/19/18 | ASG | 12405 Grant Street, Thornton CO, 80241 | https://www.meetup.com/it-ntl/events/250842844/ |
| What is AIOps? Ansible and Elasticsearch integration | 26 | Anthony Sayre | Dusty Smith | Chad Rodgers | 8/21/18 | Code Talent | 3412 Blake St · Denver, CO | https://www.meetup.com/it-ntl/events/253353524/ |
| Ansible/AnsibleTower workshop | 17 | Anthony Sayre | Dusty Smith | Chad Rodgers | 10/16/18 | Starz | 8900 Liberty Cir · Englewood | https://www.meetup.com/it-ntl/events/254761210/ |
| What is AIOps? Ansible and Elasticsearch integration | 17 | Anthony Sayre | Dusty Smith | Chad Rodgers | 10/17/18 | Starz | 8900 Liberty Cir · Englewood | https://www.meetup.com/it-ntl/events/254761210/ |
